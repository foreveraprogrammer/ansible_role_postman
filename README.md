# Purpose
This is an ansible role that installs the following software
* Postman

# To Install Role

## Create the playbook and folder structure
  You can look at https://bitbucket.org/foreveraprogrammer/ansible-galaxy/src/master/ for a sample playbook and folder stucture. 


## Install Ansible
  You must have ansible installed on your computer. You can install ansible by using the following commands:
```
  sudo easy_install pip
  sudo pip install ansible --quiet
```

## Create requirement.yml file
  Create a requirements.yml that has the following text in it.
```
   - src: git+https://KiraRenee@bitbucket.org/foreveraprogrammer/ansible_role_postman.git  
     version: master
```

## Run Ansible Galaxy to install role
  Run ansible-galaxy install -r requirements.yml to install the role

# Run the Ansible Roles
ansible-playbook -K -c local -i inventory playbook.yml


